package exec.application;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.Domain.data.Constants;
import exec.FrameworkExecutionTestNG;
import exec.keywords.FrameworkKeywords;

public class iOSNativeApp extends FrameworkiOSNativeApp {
	
	/*
	 * Function name: verifyTableItems
	 * Purpose: Verify all table rows contain data in order specified in Data coulmn 
	 * Input Parameters:objectName [String]: Get data from Object column from excel sheet
	 					objectType [String]: Get data from Locator column from excel sheet
	 					      data [String]: Get data from Data column from excel sheet
	 * Output Parameters:Constants.RESULT_PASS
	 * Author: Abhinav
	 */	
	
	public static String verifyTableItems(String objectName,String objectType,String data) 
	{
		String[] tableData = null;
	   // Keywords.driver.navigate().refresh();
		if(data.contains(":"))
		 tableData = data.split(":");
		else
		 tableData[0] = data;
	   
	    int rowno=0, matchCount = 0, rowCount = 0;
	    
	    try{

	            WebDriverWait wait = new WebDriverWait(FrameworkKeywords. driver, 10);
	           System.out.println("Wait selected for table to be populated");
	           WebElement table_search=wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//UIATableView[1]")));
	           List <WebElement> rows = table_search.findElements(By.className("UIATableCell"));
	           System.out.println("Table rows:" + rows.size());
	           FrameworkExecutionTestNG.getLogReport().logMessage("Table rows:  " + rows.size());
	           rowCount = rows.size();
	           for(WebElement row:rows)
	           {
	                         
	        	   		
	        	   			if(row.getText().trim().equalsIgnoreCase(tableData[rowno]))
	                         {
	                                
	                                matchCount++;
	        	   				    String output = "Actual Value: "+row.getText()+ " Expected Value: "+ tableData[rowno] + " for Table Row number: " + (rowno+1);
	                                System.out.println(output);
	                                FrameworkExecutionTestNG.getLogReport().logMessage(output);
	                             
	                         }
	                         rowno++;
	                         
	            } //end of for loop for reading rows
	                 
	                         

	           
	    if(matchCount==rowCount)
	    {
	    	System.out.println("Flag is true");   
	    	return Constants.RESULT_PASS + ";"+"Table items " + objectName;
	    }
	    else
	    {
	    	    	return Constants.RESULT_FAIL+" ; text not present in table ";
	    }
	           }
	    catch(Exception e)
	           {
	                  
	    				  
	    	//  Keywords.out.println(new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a").format(new Date()) + " : " + "Click on table element failed " + "ObjectValue        :" + objectName +"Exception: " + e.getMessage());
		    //     Keywords.result_details = "Click on table element failed " + "ObjectValue        :" + objectName +" Exception: " + e.getMessage();
			               
	    	return Constants.RESULT_FAIL+"; Object not found "+e.getMessage();
	                  
	           }
	    
	} 

}
