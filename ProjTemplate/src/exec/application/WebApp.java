package exec.application;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import utilities.Domain.data.Constants;
import utilities.Domain.data.TestUtil;

import com.dynamoframework.selenium.support.PageFactory.DynamoPageFactory;

import exec.ExecutionTestNG;
import exec.FrameworkExecutionTestNG;
import exec.keywords.FrameworkKeywords;

/**
 * @author Abhinav
 * Method to click on Inspection type to reflect information about an Inspection group on Inspection Manager
 *
 */


public class WebApp extends FrameworkWebApp {
	
	
	/*
	 * Function name:openBrowser
	 * Purpose: Launching new Browser.
	 * Input Parameters:browserName [String] : Browser to be Launched.
	 * Output Parameters:Constants.RESULT_PASS
	 * Author:Abhinav
	 * 
	 * Changes: Added an input parameter filePath for adding apk/ipa for mobile 
	 * Changes: Added code to run Appium from command line
	 */	
	public static InternetExplorerDriverService IEservice;
	
	public static String openBrowser(String filePath,String data, String browserName)
	{

		try
		{
		//Open application in Mozilla
			System.out.println("Open Browser in Project WebApp");
			if(browserName.equals("Mozilla"))
		{
			FrameworkKeywords.firefoxProfile = new FirefoxProfile();
			FrameworkKeywords.firefoxProfile.setPreference("security.mixed_content.block_active_content", false);
			FrameworkKeywords.firefoxProfile.setPreference("security.mixed_content.block_display_content", false);
			FrameworkKeywords.firefoxProfile.setPreference("browser.fixup.alternate.enabled", false);
			//FrameworkKeywords.firefoxProfile.setPreference("browser.fixup.alternate.suffix", "");
			//FrameworkKeywords.firefoxProfile.setPreference("browser.fixup.alternate.prefix", "");
			FrameworkKeywords.driver=new FirefoxDriver(FrameworkKeywords.firefoxProfile);
			//FrameworkKeywords.driver = new FirefoxDriver();	
			//FrameworkKeywords.driver = new FirefoxDriver(desiredCapabilities);
			FrameworkKeywords.driver.manage().window().maximize();
			
		}

		//Open application in Internet Explorer
		else if(browserName.equals("IE"))
		{
			//to get path of IEDriverServer exe from Framework resource folder
			
			File directory = new File("");
			String path = directory.getAbsolutePath();
			int pos = directory.getAbsolutePath().lastIndexOf("\\");
			String path1 = path.substring(0, pos+1);
			path1 = path1 + "Framework\\bin\\resource\\IEDriverServer.exe";
			System.out.println("Path1:" + path1);
			System.setProperty("webdriver.ie.driver", path1) ; 
			//System.setProperty(InternetExplorerDriver.LOG_FILE, "E:\\IELogs\\IEDriverServer.log");
			//System.setProperty(InternetExplorerDriver.INITIAL_BROWSER_URL, "about:blank");
		
            System.setProperty("webdriver.ie.driver",FrameworkKeywords.getProjectWS()+ "//src//resource//IEDriverServer.exe") ; 
			DesiredCapabilities capab = DesiredCapabilities.internetExplorer();
			capab.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			//capab.setPlatform(org.openqa.selenium.Platform.ANY);
			//capab.setCapability("ignoreProtectedModeSettings", true);
			capab.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			FrameworkKeywords.driver = new InternetExplorerDriver(capab);
			
			
			FrameworkKeywords.driver.manage().window().maximize();
			Thread.sleep(6000);

		}

		//Open application in Google Chrome

		else if(browserName.equals("Chrome"))
		{
			
			//to get path of chromedriver exe from Framework resource folder
			//commented as chromedriver exe is not recognized properly
			File directory = new File("");
			String path = directory.getAbsolutePath();
			int pos = directory.getAbsolutePath().lastIndexOf("\\");
			String path1 = path.substring(0, pos+1);
			path1 = path1 + "Framework\\bin\\resource\\chromedriver.exe";
			System.out.println("Path1:" + path1);

			System.setProperty("webdriver.chrome.driver",path1);
			System.setProperty("webdriver.chrome.driver",FrameworkKeywords.getProjectWS()+ "//src//resource//chromedriver.exe"); 
			//System.setProperty("webdriver.chrome.logfile", "E:\\IELogs\\chromedriver.log");
			FrameworkKeywords.driver=new ChromeDriver();
			//FrameworkKeywords.driver = new RemoteWebDriver(FrameworkKeywords.chromeService.getUrl(),DesiredCapabilities.chrome());
			FrameworkKeywords.driver.manage().window().maximize();
		}
		//changed from 30 to 5
		FrameworkKeywords. driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//page load timeout
		FrameworkKeywords. driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		FrameworkExecutionTestNG.getLogReport().logMessage("Open Browser: " + browserName);
		//adding one more line for multiple execution
		FrameworkExecutionTestNG.setObjectMap(TestUtil.setObjectMap(ExecutionTestNG.locatorFile));

		DynamoPageFactory.initElements(FrameworkKeywords.driver, FrameworkExecutionTestNG.getObjectMap());
		return Constants.RESULT_PASS + ";" + "Open Browser: " + browserName;
		}
		catch(Exception e)
		{
			FrameworkExecutionTestNG.getLogReport().logException(e);
			System.out.println("Exception: " + e.getMessage());
			return Constants.RESULT_FAIL+ ";" + e.getMessage();
		}
		
	}

  
	public static String clickTableElem(String objectName,String objectType,String data)
	{
		boolean flag = false;

		try{

			
			WebDriverWait wait = new WebDriverWait(FrameworkKeywords. driver, 20);
			
			WebElement table_search=wait.until(ExpectedConditions.visibilityOf(((WebElement) FrameworkExecutionTestNG.getObjectMap().getProperty(objectType).getValue())));

			//if element to be clicked is a link 
			
			/*try
			{
				if(FrameworkKeywords.driver.findElements(By.partialLinkText(data)).size()!=0)
				{
					FrameworkKeywords.driver.findElement(By.partialLinkText(data)).click();
					return Constants.RESULT_PASS;
				}
			}
			catch(Exception e)
			{}

			try
			{
				if(FrameworkKeywords.driver.findElements(By.linkText(data)).size()!=0)
				{
					FrameworkKeywords.driver.findElement(By.linkText(data)).click();
					return Constants.RESULT_PASS;
				}
			}
			catch(Exception e)
			{}
			*/
			List <WebElement> rows = table_search.findElements(By.tagName("tr"));
			System.out.println("Table rows:" + rows.size());

			for(WebElement row:rows)
			{
				List <WebElement> columns = row.findElements(By.tagName("td"));
				int listLength = columns.size();
				for(int i=0;i<listLength;i++)
				{

					System.out.println("Table data:" +columns.get(i).getText());
					if((columns.get(i).getText()).trim().equalsIgnoreCase(data))
					{
						flag = true;
						String output = "Actual Value: "+columns.get(i).getText()+ " Expected Value: "+data;
						System.out.println(columns.get(i+1).getText());
						WebElement elem = row.findElement(By.xpath("./td["+ (i+2) +"]/span"));
						elem.click();
						System.out.println(output);
						break;
					}
				}
				if(flag)
					break;

			}
			

			if(flag)
				return Constants.RESULT_PASS +"; Pass";
			else
				return Constants.RESULT_FAIL+"; -- text not present in table ";
		}catch(Exception e)
		{
			return Constants.RESULT_FAIL+"; Object not found "+e.getMessage();
		}

		
	}
	
	public static String clickTableElemSpan(String objectName,String objectType,String data)
	{
		boolean flag = false;

		try{

			
			WebDriverWait wait = new WebDriverWait(FrameworkKeywords. driver, 20);
			
			WebElement table_search=wait.until(ExpectedConditions.visibilityOf(((WebElement) FrameworkExecutionTestNG.getObjectMap().getProperty(objectType).getValue())));
			List <WebElement> rows = table_search.findElements(By.tagName("tr"));
			System.out.println("Table rows:" + rows.size());

			for(WebElement row:rows)
			{
				List <WebElement> columns = row.findElements(By.tagName("td"));
				int listLength = columns.size();
				for(int i=0;i<listLength;i++)
				{

					System.out.println("Table data:" +columns.get(i).getText());
					if((columns.get(i).getText()).trim().equalsIgnoreCase(data))
					{
						flag = true;
						String output = "Actual Value: "+ columns.get(i).getText() + " Expected Value: "+data;
						System.out.println(columns.get(i+1).getText());
						WebElement elem = row.findElement(By.xpath("//td["+ (i+1) +"]/span"));
						elem.click();
						System.out.println(output);
						break;
					}
				}
				if(flag)
					break;

			}
			

			if(flag)
				return Constants.RESULT_PASS +"; Pass";
			else
				return Constants.RESULT_FAIL+"; -- text not present in table ";
		}catch(Exception e)
		{
			return Constants.RESULT_FAIL+"; Object not found "+e.getMessage();
		}

		
	}
	/**
	 * @author Abhinav
	 * Method to click on cell next to an item
	 * 
	 *
	 */
	public static String clickTableNextElement(String objectName,String objectType,String data)
	{
		boolean flag = false;

		try{

			
			WebDriverWait wait = new WebDriverWait(FrameworkKeywords. driver, 20);
			
			WebElement table_search=wait.until(ExpectedConditions.visibilityOf(((WebElement) FrameworkExecutionTestNG.getObjectMap().getProperty(objectType).getValue())));
			List <WebElement> rows = table_search.findElements(By.tagName("tr"));
			System.out.println("Table rows:" + rows.size());

			for(WebElement row:rows)
			{
				List <WebElement> columns = row.findElements(By.tagName("td"));
				int listLength = columns.size();
				for(int i=0;i<listLength;i++)
				{

					System.out.println("Table data:" +columns.get(i).getText());
					if((columns.get(i).getText()).trim().equalsIgnoreCase(data))
					{
						flag = true;
						String output = "Actual Value: "+ columns.get(i).getText() + " Expected Value: "+data;
						System.out.println(columns.get(i+1).getText());
						WebElement elem = row.findElement(By.xpath("//td["+ (i+2) +"]"));
						elem.click();
						System.out.println(output);
						break;
					}
				}
				if(flag)
					break;

			}
			

			if(flag)
				return Constants.RESULT_PASS +"; Pass";
			else
				return Constants.RESULT_FAIL+"; -- text not present in table ";
		}catch(Exception e)
		{
			return Constants.RESULT_FAIL+"; Object not found "+e.getMessage();
		}

		
	}
	/*
	 * @author Abhinav
	 * Method to click on Check box inside a table row
	 *
	 */
	public static String clickCheckBoxInTable(String objectName,String objectType,String data)
	{
		boolean flag = false;

		try{

			
			WebDriverWait wait = new WebDriverWait(FrameworkKeywords. driver, 20);
			
			WebElement table_search=wait.until(ExpectedConditions.visibilityOf(((WebElement) FrameworkExecutionTestNG.getObjectMap().getProperty(objectType).getValue())));

			List <WebElement> rows = table_search.findElements(By.tagName("tr"));
			System.out.println("Table rows:" + rows.size());

			for(WebElement row:rows)
			{
				List <WebElement> columns = row.findElements(By.tagName("td"));
				int listLength = columns.size();
				for(int i=0;i<listLength;i++)
				{

					System.out.println("Table data:" +columns.get(i).getText());
					if((columns.get(i).getText()).trim().equalsIgnoreCase(data))
					{
						flag = true;
						String output = "Actual Value: "+columns.get(i).getText()+ " Expected Value: "+data;
						WebElement elem = row.findElement(By.tagName("input")); //row.findElement(By.xpath("//input"));
						elem.click();
						System.out.println(output);
						break;
					}
				}
				if(flag)
					break;

			}
			

			if(flag)
				return Constants.RESULT_PASS +"; Pass";
			else
				return Constants.RESULT_FAIL+"; -- text not present in table ";
		}catch(Exception e)
		{
			return Constants.RESULT_FAIL+"; Object not found "+e.getMessage();
		}

		
	}
	
	/*
	 * 
	 * function to click on an object in a table based on table text
	 * @param objectName
	 * @param objectType
	 * @param data
	 * @return
	 */
	public static String clickObjectInTable(String objectName,String objectType,String data)
	{
		boolean flag = false;

		try{

			
			WebDriverWait wait = new WebDriverWait(FrameworkKeywords. driver, 20);
			
			WebElement table_search=wait.until(ExpectedConditions.visibilityOf(((WebElement) FrameworkExecutionTestNG.getObjectMap().getProperty(objectType).getValue())));

			List <WebElement> rows = table_search.findElements(By.tagName("tr"));
			System.out.println("Table rows:" + rows.size());

			for(WebElement row:rows)
			{
				List <WebElement> columns = row.findElements(By.tagName("td"));
				int listLength = columns.size();
				for(int i=0;i<listLength;i++)
				{

					System.out.println("Table data:" +columns.get(i).getText());
					String tblColumnData = columns.get(i).getText();
					if(columns.get(i).findElements(By.tagName("span")).size()>0)
						tblColumnData = columns.get(i).findElement(By.tagName("span")).getText();
					else
						tblColumnData = columns.get(i).getText();
					if(tblColumnData.contains("Rename"))
					{
						int index1 = tblColumnData.indexOf("Rename");
						tblColumnData = tblColumnData.substring(0, index1);
						tblColumnData = tblColumnData.trim();
						
					}
					if(tblColumnData.trim().equals(data))
					{
						flag = true;
						String output = "Actual Value: "+columns.get(i).getText()+ " Expected Value: "+data;
						try
						{
						WebElement objInTable=columns.get(i).findElement(By.tagName("button"));
						objInTable.click();
						}
						catch(Exception e)
						{
							
						}
						System.out.println(output);
						break;
					}
				}
				if(flag)
					break;

			}
			

			if(flag)
				return Constants.RESULT_PASS +"; Pass";
			else
				return Constants.RESULT_FAIL+"; -- text not present in table ";
		}catch(Exception e)
		{
			return Constants.RESULT_FAIL+"; Object not found "+e.getMessage();
		}

		
	}
	
	
	
	
	/*
	 * @author Abhinav
	 * Method to clear a text box field
	 * 
	 */
	public static String clearTextBox(String objectName,String objectType,String data)
	{
		boolean flag = false;

		try{

			
			WebDriverWait wait = new WebDriverWait(FrameworkKeywords. driver, 20);
			
			WebElement elemTextBox=wait.until(ExpectedConditions.visibilityOf(((WebElement) FrameworkExecutionTestNG.getObjectMap().getProperty(objectType).getValue())));
			if(elemTextBox.isDisplayed())
					{
						flag = true;
						elemTextBox.click();
						elemTextBox.clear();
						if(elemTextBox.getText().isEmpty())
						{}
						else
						{
							elemTextBox.sendKeys(Keys.chord(Keys.CONTROL,"a"));
							elemTextBox.sendKeys(Keys.DELETE);
						}

					}

			

			if(flag)
				return Constants.RESULT_PASS +"; Pass";
			else
				return Constants.RESULT_FAIL+"; -- text box could not be cleared ";
		}catch(Exception e)
		{
			return Constants.RESULT_FAIL+"; Object not found "+e.getMessage();
		}

		
	}
	/*
	 * @author Abhinav
	 * Click on a particular key value
	 *
	 */
	public static String hitAKey(String objectName,String objectType,String data)
	{
		boolean flag = false;

		try{

			
			WebDriverWait wait = new WebDriverWait(FrameworkKeywords. driver, 20);
			
			WebElement elemTextBox=wait.until(ExpectedConditions.visibilityOf(((WebElement) FrameworkExecutionTestNG.getObjectMap().getProperty(objectType).getValue())));
			if(elemTextBox.isDisplayed())
			{
				flag = true;
				elemTextBox.click();
				elemTextBox.sendKeys(Keys.ENTER);

			}
		if(flag)
				return Constants.RESULT_PASS +"; Pass";
			else
				return Constants.RESULT_FAIL+"; Enter key could not be hit on :" + objectType;
		}
		catch(Exception e)
		{
			return Constants.RESULT_FAIL+"; Object not found "+e.getMessage();
		}

		
	}
	/*
	 * Function name:verifyTextNotPresent
	 * Purpose: Verify link with particular text does not exist on a web page.
	 * Input Parameters:objectName [String]: Get data from Object column from excel sheet.
	     				objectType [String]: Get data from Locator column from excel sheet.
	                          data [String]: Get data from Data column from excel sheet.
	 * Output Parameters:Constants.RESULT_PASS
	 * Author:Abhinav
	 */	
	public static String verifyTextNotPresent(String objectName,String objectType,String data)
	{
		try 
		{
			//WebDriverWait wdDriverWait = new WebDriverWait(FrameworkKeywords. driver, 10);
			//WebElement Actual=wdDriverWait.until(ExpectedConditions.visibilityOf(((WebElement) FrameworkExecutionTestNG.getObjectMap().getProperty(objectType).getValue())));
			try
			{
				
				WebElement actual=FrameworkKeywords.driver.findElement(By.linkText(data));
			
			}
			catch(Exception e)
			{
			/*String getText=Actual.getText();
			System.out.println(Actual);
			String Expected=data;*/
			System.out.println(" Link Text is not present    ");
				//Reporter.log(" Text is verified    "+"Object Value:"+getText+"Expected value"+Expected);
				return Constants.RESULT_PASS;
			}


			System.out.println(" Text is present ");
			//Reporter.log(" Text is not verified "+"Actual Value:"+Actual+"Expected value"+Expected);
			return Constants.RESULT_FAIL;
			

		}
		catch (Exception e)
		{
			Reporter.log(" Driver Object is not found ");
			return Constants.RESULT_FAIL+e.getMessage();
		}

	}
	
	/*
	 * Function name:verifyTextNA
	 * Purpose: Verify text does not appear in a particular element
	 * Input Parameters:objectName [String]: Get data from Object column from excel sheet.
	     				objectType [String]: Get data from Locator column from excel sheet.
	                          data [String]: Get data from Data column from excel sheet.
	 * Output Parameters:Constants.RESULT_PASS
	 * Author:Abhinav
	 */	
	public static String verifyTextNA(String objectName,String objectType,String data)
	{
		try 
		{
			WebDriverWait wdDriverWait = new WebDriverWait(FrameworkKeywords. driver, 10);
			WebElement Actual=wdDriverWait.until(ExpectedConditions.visibilityOf(((WebElement) FrameworkExecutionTestNG.getObjectMap().getProperty(objectType).getValue())));
			String getText;
			//check if provided object is a table
			if(Actual.findElements(By.tagName("tr")).size()==0)
			{			
				System.out.println("Verification is not for table");
				if(Actual.getTagName().equalsIgnoreCase("input"))
					getText = Actual.getAttribute("value");
				else
				 getText=Actual.getText();
				System.out.println("Text from object: " + getText);
				if(!(getText.trim().equals(data.toString())))
				{
				FrameworkExecutionTestNG.getLogReport().logMessage("Text for object:  "+ objectType + " is not equal to " + data + "as expected");
				return Constants.RESULT_PASS + "; Text value for object  " + objectType + "does not match with expected " + data;
		
				}
				else
				{
					FrameworkExecutionTestNG.getLogReport().logMessage("Text for object: " + objectType + "is equal to " + data);
					return Constants.RESULT_FAIL + "; Text value for object  " + objectType + " which is " + Actual.getText() + " matches with expected " + data;
			
				}
			}
			else
			{
				Boolean flag = false;
				List <WebElement> rows = Actual.findElements(By.tagName("tr"));
				FrameworkExecutionTestNG.getLogReport().logMessage("Table rows:" + rows.size());
				System.out.println("Table rows:" + rows.size());
				if(rows.size()==0)
					 rows = Actual.findElements(By.tagName("tr"));
		
				System.out.println("Number of rows: " + rows.size());
				FrameworkExecutionTestNG.getLogReport().logMessage("Number of rows: " + rows.size());
				
				for(WebElement row:rows)
				{
					List <WebElement> columns = row.findElements(By.tagName("td"));
					for(WebElement column:columns)
					{

						System.out.println("Table data:" +column.getText());
						FrameworkExecutionTestNG.getLogReport().logMessage("Table data:" +column.getText());
						if(column.getText().isEmpty()){continue;}
						if((column.getText()).trim().equalsIgnoreCase(data))
						{
							flag = true;
							String output = "Actual Value: "+column.getText()+ " Expected Value: "+data;
							System.out.println(output);
							FrameworkExecutionTestNG.getLogReport().logMessage(output);						
							break;
						}
					}
					if(flag)
						break;

				}

				if(flag)
				{
					return Constants.RESULT_FAIL + "; Table contains value : " + data;
				}
				else
				{
					return Constants.RESULT_PASS +" ;  Table does not contain value : " + data ;
				}
				
			}
		}
		catch (Exception e)
		{
			FrameworkExecutionTestNG.getLogReport().logException(e);
			return Constants.RESULT_FAIL+" ; " + e.getMessage();
		}

	}
	
	
	/*
	 * Function name:verifyAttribute
	 * Purpose: Verify an attribute value for an object matches expected value
	 * Input Parameters:attributeName [String]: Attribute name to be verified
	     				objectType [String]: Get data from Object column from excel sheet.
	                          data [String]: Attribute value to be verified
	 * Output Parameters:Constants.RESULT_PASS
	 * Author:Abhinav
	 */	
	public static String verifyAttribute(String attributeName,String objectType,String data)
	{
		try 
		{
			//WebDriverWait wdDriverWait = new WebDriverWait(FrameworkKeywords. driver, 10);
			//WebElement Actual=wdDriverWait.until(ExpectedConditions.visibilityOf(((WebElement) FrameworkExecutionTestNG.getObjectMap().getProperty(objectType).getValue())));
			WebElement Actual=((WebElement) FrameworkExecutionTestNG.getObjectMap().getProperty(objectType).getValue());
			String value = Actual.getAttribute(attributeName).trim();
			if(value.equalsIgnoreCase(data))
			{
				FrameworkExecutionTestNG.getLogReport().logMessage("Attribute value for " + objectType + "matches expected value of: " + data);
				return Constants.RESULT_PASS + "; Attribute value for " + attributeName + "which is " + data + "matches for object " + objectType;
			}
			else
			{
				FrameworkExecutionTestNG.getLogReport().logMessage("Attribute value for " + objectType + "does not match expected value of: " + data);
				return Constants.RESULT_FAIL + "; Attribute value for " + attributeName + "which is " + data + "does not match for object " + objectType;
			}
			
			
		}
			catch(Exception e)
			{
		
				FrameworkExecutionTestNG.getLogReport().logException(e);
				return Constants.RESULT_FAIL + " ; Exception occurred : "+e.getMessage();
			}
	}
	
	
	
	/*
	 * Function name:verifyGroupUnderStandard
	 * Purpose: Verify a group is present under Standard category
	 * Input Parameters:objectName [String]: Get data from Object column from excel sheet
	 					objectType [String]: Get data from Locator column from excel sheet
	 					      data [String]: Get data from Data column from excel sheet
	 * Output Parameters:Constants.RESULT_PASS
	 * Author:Abhinav
	 */	
	public static String verifyGroupUnderStandard(String objectName,String objectType,String data) 
	{

		//  Keywords. driver.navigate().refresh();
		/* boolean flag = false;

		try{

			WebDriverWait wait = new WebDriverWait(FrameworkKeywords. driver, 20);
			WebElement table_search=wait.until(ExpectedConditions.visibilityOf(((WebElement) FrameworkExecutionTestNG.getObjectMap().getProperty(objectType).getValue())));
			List <WebElement> rows = table_search.findElements(By.xpath("./(*)/tr[contains(@class,'master-row')]"));
			FrameworkExecutionTestNG.getLogReport().logMessage("Table rows:" + rows.size());
			System.out.println("Table rows:" + rows.size());
			if(rows.size()==0)
				 rows = table_search.findElements(By.tagName("tr"));
			
			System.out.println("Number of rows: " + rows.size());
			FrameworkExecutionTestNG.getLogReport().logMessage("Number of rows: " + rows.size());
			
			for(WebElement row:rows)
			{
				List <WebElement> columns = row.findElements(By.tagName("td"));
				for(WebElement column:columns)
				{

					System.out.println("Table data:" +column.getText());
					FrameworkExecutionTestNG.getLogReport().logMessage("Table data:" +column.getText());
					if(column.getText().isEmpty()){continue;}
					if((column.getText()).trim().equalsIgnoreCase(data))
					{
						flag = true;
						String output = "Actual Value: "+column.getText()+ " Expected Value: "+data;
						System.out.println(output);
						FrameworkExecutionTestNG.getLogReport().logMessage(output);						
						break;
					}
				}
				if(flag)
					break;

			}

			if(flag)
			{
				return Constants.RESULT_PASS + "; Group name: " + data + " present under Standard Category";
			}
			else
			{
				return Constants.RESULT_FAIL+" ;  -- text not present in table ";
			}
		}catch(Exception e)
		{
			FrameworkExecutionTestNG.getLogReport().logException(e);
			return Constants.RESULT_FAIL+" Object not found "+e.getMessage();
		}*/
		boolean flag = false;

		try{

		  WebElement groupCheck=FrameworkKeywords.driver.findElement(By.xpath("//span[text()=' Standard ']/ancestor::tr/following-sibling::tr[contains(@class,'detail-row')][1]//div[contains(@class,'inspection-groups-grid')]/div[contains(@class,'grid-content')]/table//tr[contains(@class,'master-row')]//span[text()=' "+data+" ']"));
			if(groupCheck.isDisplayed())
			{
						flag = true;
						FrameworkExecutionTestNG.getLogReport().logMessage("Inspection group is added under Standard Category");
						

			}

			if(flag)
				return Constants.RESULT_PASS +"; Pass - Inspection group is added under Standard Category";
			else
				return Constants.RESULT_FAIL+"; Inspection group is not added under Standard Category";
		}
		catch(NullPointerException e)
		{
			return Constants.RESULT_FAIL +"; Inspection group is not added under Standard Category  "+ data;
		}
		catch(NoSuchElementException e)
		{
			return Constants.RESULT_FAIL +"; Inspection group is not added under Standard Category " + data;
		}
		catch(Exception e)
		{
			FrameworkExecutionTestNG.getLogReport().logException(e);
			return Constants.RESULT_FAIL+" Exception occurred:  "+e.getMessage();
		}
		

	}  
	
	     /*
		 * Function name:rightClickVerifyOption
		 * Purpose: Verify if an option is available in context menu
		 * Input Parameters:objectName [String]: Get data from Object column from excel sheet.
		     				objectType [String]: Get data from Locator column from excel sheet.Element on which right click must be applied
		                          data [String]: Get data from Data column from excel sheet. Option to select from context menu
		 * Output Parameters:Constants.RESULT_PASS
		 * Author:Abhinav
		 */	
	
		public static String rightClickVerifyOption(String objectName,String objectType,String data)
		{
			Boolean flag = false;
			try 
			{
				WebDriverWait wdDriverWait = new WebDriverWait(FrameworkKeywords.driver, 10);
				WebElement Actual=wdDriverWait.until(ExpectedConditions.visibilityOf(((WebElement) FrameworkExecutionTestNG.getObjectMap().getProperty(objectType).getValue())));
				//do right click operation
				Actions act = new Actions(FrameworkKeywords.driver);
				act.contextClick(Actual).build().perform();
				FrameworkExecutionTestNG.getLogReport().logMessage("Right click operation performed on Object : " +  objectType);
				if(data.isEmpty())
				{
					System.out.println("No option specified - to be verified from context menu");
					return Constants.RESULT_FAIL + ";" + "No data specified to be selected from context menu";
				}
				//context menu element 
				WebElement contextmenuList = FrameworkKeywords.driver.findElement(By.xpath("//div[@class='k-animation-container']/ul"));
				//list all the items
				List<WebElement> items = contextmenuList.findElements(By.xpath("./li[not(contains(@class,'ng-hide'))]"));
			    for(WebElement item:items)
			    {
			    	System.out.println(item.getText());
			    	FrameworkExecutionTestNG.getLogReport().logMessage("Items in Context menu list: ");
			    	FrameworkExecutionTestNG.getLogReport().logMessage(item.getText() +"\n");
					    	
			    }
			    for(WebElement item:items)
			    {
			     if(data.trim().equals(item.getText().trim()))
			     {
			    	 flag = true;
			    	// item.click();
			    	 break;
			     }
			    }
			    if(flag)
			    {
					return Constants.RESULT_PASS +"; Pass - Context menu item - " + data + "is available";
			    }
				else
				{
					return Constants.RESULT_FAIL+";  Fail - Context menu item - " + data + "is not available";
				}
		
				
				

			}
			catch (Exception e)
			{
				FrameworkExecutionTestNG.getLogReport().logException(e);
				return Constants.RESULT_FAIL+ ";" + e.getMessage();
			}

		}
		
}
		
		
		
		