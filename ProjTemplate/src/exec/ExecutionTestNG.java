package exec;

import java.io.File;
import java.lang.annotation.Annotation;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;

import utilities.Domain.BO.ConfigBO;
import utilities.Domain.BO.CustomAnnotation;
import exec.keywords.FrameworkKeywords;
import exec.keywords.Keywords;



/**
 * @author Abhinav
 * Execute FrameworkExecutionTestNG beforeRun method to execute tests
 * Move all logs from reports/actual to reports/archive
 * Clear all logs in reports/actual
 *
 */
public class ExecutionTestNG extends FrameworkExecutionTestNG {
    
	
	
	public static String locatorFile;
	@Override
	public void beforeRun() 
	{
		
		try {
			//move all reports from actual to archive folder on local rep
			String reportPathActual = FrameworkKeywords.getProjectWS() + "//reports//actual//";
			String reportPathArchive = FrameworkKeywords.getProjectWS() + "//reports//archive//";
			
			File trgDir = new File(reportPathArchive);
			File srcDir = new File(reportPathActual);
			FileUtils.copyDirectory(srcDir, trgDir);
			FileUtils.cleanDirectory(srcDir);
			System.out.println("Reports moved to archive!!!");
			//if logs are present move to archive
			String notePathActual = FrameworkKeywords.getProjectWS() + "//logs//actual//";
			String notePathArchive = FrameworkKeywords.getProjectWS() + "//logs//archive//";
			
			File trgDirs = new File(notePathArchive);
			File srcDirs = new File(notePathActual);
			FileUtils.copyDirectory(srcDirs, trgDirs);
			FileUtils.cleanDirectory(srcDirs);
			System.out.println("Logs moved to archive!!!");
			
			super.beforeRun();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	}
	
	public void updateEnvironmentResourceFile(String args[])
	{
		String applicationpref = "",excelfile = "",locatorfile = "",filePath = "",suitereportname="",testreportname="";
		//read command line arguments
		int lenArgs = args.length;
		//check if any arguments are passed
		if(args.length>0)
		{
		for(int i=0;i<lenArgs;i++)
		{
			switch(args[i])
			{
				case "WEB" : applicationpref = "WebApp";
				excelfile = "/data/Inspect.xlsx";
				locatorfile = "/data/Inspect.xlsx";
				suitereportname="Inspect Web";
				testreportname="Detailed Test Report for Inspect Web";
				break;
				case "ANDROID": applicationpref = "AndroidApp";
				excelfile = "/data/AndroidInspect.xlsx";
				locatorfile = "/data/AndroidInspect.xlsx";
				suitereportname="Inspect App - Android";
				testreportname="Detailed Test Report for Inspect App Android";
				break;
				case "IOS": applicationpref = "iOSNativeApp";
				excelfile = "/data/IOSInspect.xlsx";
				locatorfile = "/data/IOSInspect.xlsx";
				suitereportname="Inspect App - IOS";
				testreportname="Detailed Test Report for Inspect App IOS";
				break;
				
			 
			}
 			//read environment xml location
			Annotation[] annotations = ConfigBO.class.getAnnotations();

			for(Annotation annotation : annotations){
			    if(annotation instanceof CustomAnnotation){
			    	CustomAnnotation myAnnotation = (CustomAnnotation) annotation;
			       // System.out.println("value: " + myAnnotation.filePath());
			          filePath =  myAnnotation.filePath();
			        // System.out.println("File Path for environment :" + filePath);
			    }
			}
			
			filePath = FrameworkKeywords.getProjectWS() + filePath;
			//read xml file and update xml parameters with necessary values
			File file = new File(filePath);
			file.setReadable(true);
			file.setWritable(true);
			try
			{
		
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(file);
			doc.normalizeDocument();
			//create xpath to update above created parameters
			doc.getElementsByTagName("excelfile").item(0).setTextContent(excelfile); 
			
			doc.getElementsByTagName("locatorfile").item(0).setTextContent(locatorfile); 
			
			doc.getElementsByTagName("applicationpref").item(0).setTextContent(applicationpref); 
			
			//updating suite report and test report names
			
			doc.getElementsByTagName("suitereportname").item(0).setTextContent(suitereportname); 
			
			doc.getElementsByTagName("testreportname").item(0).setTextContent(testreportname); 
			

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath));
			transformer.transform(source, result);
			
						
			}
			catch(Exception e)
			{
				System.out.println(e.getMessage());
			}
		}//if for arg length check
			} //for loop reading args values from main method
		System.out.println("Resource File saved!");

	}
	public static void main(String args[])
	{
		ExecutionTestNG exec = new ExecutionTestNG();
		exec.updateEnvironmentResourceFile(args);
		exec.beforeRun();
		//added to terminate java class gracefully
		System.exit(0);
	}
}
